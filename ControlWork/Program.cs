﻿using System;
using System.IO;

namespace ControlWork
{
    class Program
    {
        const int max = 100;

        static void Main(string[] args) //solves the task
        {
            //defining variables to store data in
            const string data = "Streets.txt";
            Street[] S = new Street[max];
            int count;
            Read(data, S, out count);            
            int shortest = Min(S, count);
            double average = Average(S, count);
            int aboveAve = Amount(S, count, average); 

            // Printing out the results
            Print(S, count);
            Console.WriteLine("The name of the shortest street is: {0}", S[shortest]._name);
            Console.WriteLine("Average length of the streets is: {0}", average);
            Console.WriteLine("There are {0} street(s) longer than average street length.", aboveAve);
        }

        public static void Read(string data, Street[] S, out int count) //Reads from the file data
        {
            using (StreamReader sr = new StreamReader(data))
            {
                count = int.Parse(sr.ReadLine());
                string line;

                for (int i = 0; i < count; i++)
                {
                    line = sr.ReadLine();
                    string[] parts = line.Split(' ');
                    S[i] = new Street(parts[0], double.Parse(parts[1]));
                }
            }
        }

        public static void Print(Street[] S, int count) //Prints initial data
        {
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine("{0} {1}", S[i]._name, S[i]._length);
            }
        }

        public static int Min(Street[] S, int n) //Finds index of the shortest
        {
            double shortest = double.MaxValue;
            int index = 0;

            for (int i = 0; i < n; i++)
            {
                if (shortest > S[i]._length)
                {
                    shortest = S[i]._length;
                    index = i;
                }
            }

            return index;
        }

        public static double Average(Street[] S, int n) //Finds the average
        {
            double sum = 0;

            for (int i = 0; i < n; i++)
            {
                sum = sum + S[i]._length;
            }

            return sum / n;
        }

        public static int Amount(Street[] S, int n, double ave) //Finds the quantity of streets that are above average
        {
            int count = 0;

            for (int i = 0; i < n; i++)
            {
                if (S[i]._length > ave)
                {
                    count++;
                }
            }

            return count;
        }
    }
}
