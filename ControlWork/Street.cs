﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControlWork
{
    class Street
    {
        private string name;
        public string _name
        {
            get { return name; }
        }

        private double length;
        public double _length
        {
            get { return length; }
        }

        public Street()
        {
            this.name = "Empty";
            this.length = 0;
        }

        public Street(string name, double length)
        {
            this.name = name;
            this.length = length;
        }
    }
}
